<?php
//phpinfo();

function d($var){
    echo "<pre>";
    print_r($var);
    //echo__LINE__;
    echo "</pre>";
}

//What is an array
//How to define/declare and array : numeric

//declaration using array() function

/**$fruits = array(
0=>"F1",
1=>"F2",
2=>"F3");

d($fruits);

//declaration using array literal

//How to define/declare an array : associative
$fruits = array[
"Table"=>"F1",
"Chair"=>"F2",
"Board"=>"F3"];

d($fruits);*/

$fruits = [
    "Table"=>array("T1","T2","T3"),
    "chair"=>["C1","C2","C3","C4","C5"],
    "Board"=>"F3"
];

d($fruits);
echo $fruits; //array
echo $fruits["Table"]; //array
echo $fruits["Chair"]; //array
echo $fruits["Board"]; //scalar element


$fruits[77]= "Mango" ;d($fruits);
$fruits[200]= "Banana" ;d($fruits);
$fruits[]= "Watermelon ;d($fruits);
$fruits[99]= "Apple" ;d($fruits);
$fruits[]= "Orange" ;d($fruits);

//How to access an element of an array : numeric
echo $fruits[99];
echo $fruits[201];

//Resign
$fruits[99] = "Hello World"; d($fruits);
//first element 
echo $fruits["Board"];
//last element
echo count ($fruits);
echo end($fruits);
echo current($fruits);











?>